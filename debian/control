Source: nest-simulator
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Graber <s.graber@fz-juelich.de>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               cmake,
               python3-dev,
               libltdl-dev,
               libreadline-dev,
               libncurses-dev,
               libgsl-dev,
               openmpi-bin,
               libopenmpi-dev,
               libgomp1,
               libomp-dev,
               libibverbs-dev,
               libpcre3,
               libpcre3-dev,
               jq,
               python3-numpy,
               python3-pandas,
               python3-scipy,
               python3-matplotlib,
               python3-mpi4py,
               python3-setuptools,
               python3-tk,
               python3-pydot,
               cython3,
               libtool
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/nest-simulator
Vcs-Git: https://salsa.debian.org/med-team/nest-simulator.git
Homepage: https://github.com/nest/nest-simulator/

Package: nest-simulator
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: The Neural Simulation Tool - NEST
 NEST is a simulator for spiking neural network models that focuses on
 the dynamics, size and structure of neural systems rather than on the
 exact morphology of individual neurons. The development of NEST is
 coordinated by the NEST Initiative. General information on the NEST
 Initiative can be found at its homepage at https://www.nest-
 initiative.org.
 .
 NEST is ideal for networks of spiking neurons of any size, for example:
 .
  * Models of information processing e.g. in the visual or auditory
    cortex of mammals,
  * Models of network activity dynamics, e.g. laminar cortical networks
    or balanced random networks,
  * Models of learning and plasticity.
